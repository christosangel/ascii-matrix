# ascii-matrix


This program written in the C language, will render the matrix effect in the
terminal, while rendering ascii art loaded from a txt file,
at the center of the terminal window.

[![video](screenshots/video.png){height=300}]( https://youtu.be/mG9BiZpS7CU)

## HOW TO CREATE SUITABLE ASCII-ART txt FILES

This program renders ascii-art txt files that are contain these characters: " (white space), 1,2,3).

In order to produce this kind of txt files, the program **jp2a** is used ([https://github.com/cslarsen/jp2a](https://github.com/cslarsen/jp2a)).

To install this program in Debian-base OSs:

```
apt-get install jp2a
```

To generate an ascii-art txt file from a png image, in order to use it with ascii-matrix, run the following command:

```
jp2a path/to/image.png --chars="01234567" --height=20>path/to/output.txt

```
In order to get ascii-art of a word/small text, there are two solutions:
 * Use `convert`:

 ```
convert -background white -fill black -pointsize 50 label:"Word here"  word.png
 ```

* Take a screenshot of the word, and work with it.

**Since there are only two colors in such image, and the geometry of the image is different, the flags of the following `jp2a` should be adjusted accordingly:**

```
 jp2a word.png --chars="01" --height=15>word.txt

```
__NOTICE:__ As many times the output of the `jp2a` command is not optimal, the user is encouraged to edit the txt file using a text editor, in order to get the desired result.

## INSTALL ascii-matrix

*   You can run the command:

```
git clone https://gitlab.com/christosangel/ascii-matrix.git
```

or

*   visit  [https://gitlab.com/christosangel/asciii-matrix.git]( https://gitlab.com/christosangel/asciii-matrix.git)
*   Click on the *Download* button to download the zip, then unzip, and
*    once inside the ascii-matrix directory (`cd ascii-matrix` or `cd ascii-matrix-main`)
* Compile the ascii-matrix.c, in order to create the executable:


```
gcc ascii-matrix.c -Wall -o ascii-matrix
```

After that, copy the executable to ` ~/.local/bin/`:


```
cp ascii-matrix ~/.local/bin/

```

Provided that ` ~/.local/bin/` is added to the `$PATH`, you can run `ascii-matrix`  from any directory.

You are now ready to go!

## USAGE

Once you have generated the ascii-art txt file that you want to show, open a terminal window (__max size 540x134__), and run:

```
ascii-matrix -f path/to/ascii.txt
```

You may want to add the flags that you prefer in order to have an outcome of your liking.


## FLAGS

The user can either use the short or the long flag version
(i.e. ascii-matrix -h and ascii-matrix --help are the same).

|n| Short flag| Long flag| Explanation|
|---|------|---------|---------|
|1|`-h`| `--help`|	Shows this help text.|
|2|`-f`| `--file`|	Defines the path to the ascii txt file to render in the center of the matrix window. If this flag isn't used, the **command will still render a matrix screen with no ascii art**.
|3|`-s`| `--speed`  |	Defines the speed of the matrix falling digits.	**Acceptable values 0-9** (default: 5).|
|4|`-d`|`--dense`  |	Defines the density of the matrix lines. **Acceptable values 0-9** (default: 5).|
|||
|5|`-m1`|`--matrix-color1`  |	Defines the color of first matrix digits (default: yellow).|
|6|`-m2`|`--matrix-color2 `|	Defines the color of matrix digits (default: green).|
|7|`-c1`|`--ascii-color1`|  The first color of the ascii art (default: blue).  |
|8|`-c2`|`--ascii-color2`  | The second color of the ascii art (default: red).|
|9|`-c3`|`--ascii-color3`  | The third color of the ascii art (default: white). |
|10|`-c4`|`--ascii-color3`  | The fourth color of the ascii art (default: yellow). |
|11|`-c5`|`--ascii-color3` | The fifth color of the ascii art (default: green). |
|12|`-c6`|`--ascii-color6`  | The sixth color of the ascii art (default: grey).|
|13|`-c7`|`--ascii-color7`  | The seventh color of the ascii art (default: black). |
|||	**Acceptable color values: black, maroon, green, olive, navy, purple, teal, silver, grey, red, lime, yellow, blue, fuchsia, aqua and white.** These colors are defined by the color profile of your terminal. |
|||
|14|`-i`|`--invert` |	Inverts the colors of the ascii art (background-foreground).  |
|||
|15|`-a1`|`--ascii1` | Defines the characters for the first category of the ascii art (default: L).|
|16|`-a2`|`--ascii2` | Defines the characters for the second category of the ascii art (default: L).|
|17|`-a3`|`--ascii3`|Defines the characters for the third category of the ascii art (default: L).|
|18|`-a4`|`--ascii4` | Defines the characters for the fourth category of the ascii art (default: L).|
|19|`-a5`|`--ascii5` | Defines the characters for the fifth category of the ascii art (default: L).|
|20|`-a6`|`--ascii6`|Defines the characters for the sixth category of the ascii art (default: L).|
|21|`-a7`|`--ascii7` | Defines the characters for the seventh category of the ascii art (default: L).|
|||	**Acceptable -a{1..7} values :**|
|||		**L** Random uppercase letters [A-Z]|
|||		**l** Random lowercase letters [a-z]|
|||		**n** Random numbers [0-9]|
|||		**s** Space (best used in combination with **-i** or **-d 9**)|
|||		**X** the letter **X**|
|||		**p** Random punctuation marks **'()\*+,-./**|
|||


---

---
Some sample ascii txt files were added to the repo, just to help with examples of usage.

## Examples

---

```
ascii-matrix
```
will render a matrix window with the default matrix colors.

![1.gif](screenshots/1.gif){height=300}

_Only png screenshots are displayed instead of gifs, in order to make repo lighter to load and download._

---


```
ascii-matrix -f ubuntu.txt
```

will render the ubuntu.txt ascii, with with the default values.

![ubuntu.png](screenshots/ubuntu.png){height=300}


---

```
ascii-matrix -f xfce.txt -c1 grey -c2 aqua -c3 white -a1 l -a2 n -a3 L -m1 yellow -m2 green
```

will render the xfce.txt ascii, with white uppercase letters, grey lowercase letters and aqua punctuation marks, in a matrix with yellow and green digits.

![xfce.png](screenshots/xfce.png){height=300}

---

```
ascii-matrix -f archlinux.txt -c1 navy -c2 aqua -c3 grey -a1 s -a2 s -a3 s -i
```
will render the archlinux.txt ascii in spaces,with the defined colors inverted.

![arch.png](screenshots/arch.png){height=300}


---

```
ascii-matrix -f mint1.txt -d 9 -a1 s -a2 s -a3 s
```

will render the mint1.txt ascii with spaces, in a fully dense lined matrix screen.

![mint1.png](screenshots/mint1.png){height=300}

---

```
ascii-matrix -f fedora.txt -c1 aqua -a1 L -a2 p -a3 s  -s 8

```

will render the fedora.txt ascii in aqua, in a rapid matrix screen.

![fedora.png](screenshots/fedora.png){height=300}

---

```
ascii-matrix -f mint2.txt -c1 grey -c2 green -a1 s -a2 s -i

```

will render the mint2.txt ascii in green and grey spaces inverted.

![mint2.png](screenshots/mint2.png){height=300}



---

```
ascii-matrix -f lemmy.txt -a1 s -d 9

ascii-matrix -f mastodon.txt -a1 s -d 9
```

![lemmy.png](screenshots/lemmy.png){height=200}
![mastodon.png](screenshots/mastodon.png){height=200}

---

```
ascii-matrix -f thelinuxcast.txt -c1 red -c2 black -c3 white -i -a1 s -a2 s -a3 s

ascii-matrix -f slack.txt
```

![thelinuxcast.png](screenshots/linuxcast.png){height=200}
![slack.png](screenshots/slack.png){height=200}


---

```
ascii-matrix -f mx.txt  -a1 p -i

ascii-matrix -f tumbleweed.txt  -m1 red -m2 blue -a1 n -c1 yellow
```

![mx.png](screenshots/mx.png){height=200}
![tumbleweed.png](screenshots/tumbleweed.png){height=200}


---
```
ascii-matrix -f kafeneio.social.txt  -a1 s  -m1 maroon -m2 fuchsia -i -s 7 -c1 aqua -d 9

ascii-matrix -f zoidberg.txt -c3 white -c2 black -c1 red -a1 s -a2 s -a3 s -i -m1 yellow -m2 blue
```

![kafeneio.social.png](screenshots/kafeneio.png){height=200}
![zoidberg.png](screenshots/zoidberg.png){height=200}

---
```
ascii-matrix -f bender.txt -c3 white -c2 black -c1 aqua -a1 s -a2 s -a3 s -i -m1 yellow -m2 red

ascii-matrix -f tux.txt -c1 yellow -c2 white -c3 black -i -a1 s -a2 s -a3 s -m1 silver -m2 blue
```

![bender.png](screenshots/bender.png){height=200}
![tux.png](screenshots/tux.png){height=200}

---

```
ascii-matrix -f noface1.txt -a1 s -a2 s -a3 s -i -c1 white -c2 red -c3 black

ascii-matrix -f noface2.txt -a1 s -a2 s -a3 s -i -c1 white -c2 black -c3 red

ascii-matrix -f jolly-roger.txt -c1 white -a1 s -i -a2 s

ascii-matrix -f endeavouros.txt -c1 maroon -c2 purple -c3 teal -a1 n -a2 L -a3 l -m1 blue -m2 purple

```

![noface1.png](screenshots/noface1.png){height=150}
![noface2.png](screenshots/noface2.png){height=150}
![jolly-roger.png](screenshots/jolly-roger.png){height=300}
![endeavouros.png](screenshots/endeavouros.png){height=300}

---

---
Feel free to **submit your ascii-art txt files**, in order to enrich this small collection, making it available to many others.
